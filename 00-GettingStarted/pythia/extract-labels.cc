
#include "Pythia8/Pythia.h"
#include <locale>

using namespace Pythia8;

//--------------------------------------------------------------------------

// Small helper function to get variation names.

string weightLabel(string weightString) {
  // Strip leading whitespace
  weightString.erase(0,weightString.find_first_not_of(" \t\n\r\f\v"));
  // Find first blank and use this to isolate weight label.
  int iBlank = weightString.find(" ", 0);
  return weightString.substr(0, iBlank);
}

//--------------------------------------------------------------------------

int main(int, char* argv[] ) {

  // Redirect output so that Pythia banner will not be printed twice.
  std::streambuf *old = cout.rdbuf();       
  stringstream ss;
  cout.rdbuf (ss.rdbuf());
  std::locale loc;

  Pythia pythia;
  pythia.readFile(argv[1]);
  pythia.init();

  // Restore print-out.
  cout.rdbuf (old);

  // Extract weight variation labels from Pythia
  vector<string> weightStrings = pythia.settings.wvec("UncertaintyBands:List");
  for (int iWeight=0; iWeight <= weightStrings.size(); ++iWeight) { 
    cerr << weightStrings[iWeight-1] << endl;
    string filename = (iWeight==0)
      ? "baseline.hepmc" : weightLabel(weightStrings[iWeight-1]) + ".hepmc";
    for (auto elem : filename) {
      cout << std::tolower(elem,loc);
    }
    cout << "\n";
  }

  return 0;

}
