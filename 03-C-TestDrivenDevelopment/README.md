# Advanced material: Test Driven Development

Test-driven development (TDD) is a software development methodology that leverages writing tests first 
to achieve correct programs, cleaner code, and more modular design. Although the basics are surprisingly easy, 
potential new adopter often struggle in adopting the technique or even fail to see the point. Having been a sceptic 
myself many years ago, I would like to introduce you to test-driven development in a safe, hands-on environment.

The workshop consists of a brief introduction to the basic TDD development cycle followed by an extended hands-on 
session. You will work on a simple programming assignment putting the TDD cycle into practice. During this phase, 
I will provide guidance to what you can improve and answer any questions that you may have. The workshop concludes 
with a discussion section on the pros and cons of TDD.

For maximum effectiveness, please bring a Laptop computer with a working development environment that can execute 
unit tests. TDD can be practised in virtually any programming language, so you can pick the language you are most 
familiar with. I have prepared sample projects for the Python and C++ languages; if you have Python (with 
virtualenv or pyenv) or a C++ installer (with cmake) installed, there is nothing you need to worry about before 
the workshop starts.

If you do not have a Laptop of your own, it may still be worthwhile to join the workshop 
since we will join up in pairs for the hands-on session anyways.

