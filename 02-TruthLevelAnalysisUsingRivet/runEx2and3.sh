#!/usr/bin/env bash

rivet --skip-weights --pwd -a MY_ANALYSIS:LMODE=EL -H output_el.yoda myZjetsEvents10k.hepmc2g
rivet --skip-weights --pwd -a MY_ANALYSIS:LMODE=MU -H output_mu.yoda myZjetsEvents10k.hepmc2g
