#!/usr/bin/env bash

rivet-mkhtml --remove-options --errs -o plots_ex5 \
output_inv_strict.yoda:"Title=invisibles only" \
output_inv_detect.yoda:"Title=invisibles and out-of-acceptance muons"
