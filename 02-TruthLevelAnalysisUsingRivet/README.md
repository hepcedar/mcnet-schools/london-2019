# Rivet tutorial

## Environment Setup

For this tutorial we'll be using Sherpa and Rivet. We have prepared a dedicated docker
container for you which you should pull as follows:

        $ docker pull hepstore/rivet-sherpa:3.0.1-2.2.7

Once you've pulled the container, source the setup script in this directory:

        $ source setup.sh

This will alias the relevant commands to use the executables from within the container
without having to enter the container environment.

Alternatively, you can also run the exercises from within the container, in which case 
you do not need to source the setup script and instead run

        $ docker run -it -u $(id -u $USER) -v $PWD:$PWD -w $PWD hepstore/rivet-sherpa:3.0.1-2.2.7 bash

On Windows 10 laptops, you can start the docker run with

        $ docker run -it -u 1000 -v %cd%:/home -w /home hepstore/rivet-sherpa:3.0.1-2.2.7

The operating system will then ask you for permission to access the shared folder.



Now you're ready to start with the first exercise!

