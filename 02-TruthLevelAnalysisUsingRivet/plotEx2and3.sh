#!/usr/bin/env bash

rivet-mkhtml --remove-options --errs -o plots_ex2and3 \
output_mu.yoda:"Title=\$pp\\to\\mu^+\\mu^-+0,1j\$@LO" \
output_el.yoda:"Title=\$pp\\to e^+e^-+0,1j\$@LO"
