#!/usr/bin/env bash

DBLUE="blue!90!white"
MBLUE="blue!60!white"
LBLUE="blue!30!white"
COMMON_TAGS="RatioPlotSameStyle=1:ErrorBars=0:ErrorBands=1:ErrorBandOpacity=0.5"
MEPS_STYLE=$COMMON_TAGS:ErrorBandColor=$LBLUE:LineColor=$LBLUE
ME_STYLE=$COMMON_TAGS:ErrorBandColor=$MBLUE:LineColor=$MBLUE
STAT_STYLE=$COMMON_TAGS:ErrorBandStyle=hlines:ErrorBandColor=$DBLUE:LineColor=$DBLUE

rivet-mkhtml --errs -o plots_ex4 \
output_multiweights_full_band.yoda:$MEPS_STYLE:"Title=ME+PS scales \$\\oplus\$ stats" \
output_multiweights_stat_MEonly.yoda:$ME_STYLE:"Title=ME scales \$\\oplus\$ stats" \
output_multiweights_stat_only.yoda:$STAT_STYLE:"Title=stats only"

