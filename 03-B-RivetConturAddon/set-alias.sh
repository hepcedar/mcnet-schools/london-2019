alias	rivet='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur rivet'
alias	rivet-mkanalysis='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur rivet-mkanalysis'
alias	rivet-buildplugin='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur rivet-buildplugin'
alias	rivet-mkhtml='docker run -i  --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur rivet-mkhtml'

alias	docker-make='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur make'
alias   ufo2herwig='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur ufo2herwig'
alias   contur='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur contur'
alias	batch-submit='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD  -w $PWD  huangjoanna/contur batch-submit'
alias	contur-plot='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur contur-plot'
alias	Herwig='docker run -i --rm  -u `id -u $USER`:`id -g`  -v $PWD:$PWD -w $PWD  huangjoanna/contur Herwig'

