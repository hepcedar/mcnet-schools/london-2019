# B-L gauged model

As used in [these studies](https://contur.hepforge.org/results/BL3/index.html)

The file BL3limits.py contains functions used to plot the experimental and theoretical limits on the plots.

## Model Authors
Wei Liu, Frank Deppisch
