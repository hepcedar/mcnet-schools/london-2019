## Write a Parton Shower using Python

# Uncertainties and matching for the Python parton shower

The worksheet for both introductory and advanced PS development
tutorials is shared. For the advanced part, you should try to complete
sections 6-10 in the worksheet.

# Docker container

It should be straight-forward to work on the tutorial on your personal
machine. You may also use the docker container for the Pythia
tutorial (which e.g. includes pypy) for the tutorial. For that container,
please consult http://home.thep.lu.se/~prestel/Tutorials.html about the setup.
The worksheet is available from bitbucket (i.e. here).
